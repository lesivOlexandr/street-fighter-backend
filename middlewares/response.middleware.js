const { messages } = require('../constants/error-messages');

const responseMiddleware = (req, res, next) => {
    if (!res.data) {
        throw {message: messages.CriticalError, status: 404 }
    } else {
        res.status(200).send(res.data);
    }
    next()
}

exports.responseMiddleware = responseMiddleware;