const { fighter } = require('../models/fighter');
const { validateFighter, validateFighterPartial } = require('../validators/fighter.validators');

const createFighterValid = (req, res, next) => {
    const error = validateFighter(req.body);
    if(error) {
        throw { message: error, status: 400 };
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    const error = validateFighterPartial(req.body);
    if(error) {
        throw { message: error, status: 400 };
    }
    
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;