const { user } = require('../models/user');
const { messages } = require('../constants/error-messages');

const { validateUser, validateUserPartial } = require('../validators/user.validators');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const error = validateUser(req.body, next);
    if (error) {
        throw { message: error, status: 400 };
    }

    // Проверяем есть ли в нашей базе данных пользователь с таким email или номером телефона.
    // В настоящей базе данных конечно же нужно было б использовать встроенный функционал базы,
    // но в нашем JSON-хранилище перебор похоже - единственный вариант
    if (UserService.search({ email: req.body.email })) {
        throw { message: messages.UserEmailNotUnique, status: 400 };
    } else if (UserService.search({ phoneNumber: req.body.phoneNumber })){
        throw { message: messages.UserPhoneNumberNotUnique, status: 400 };
    }

    next();
}

const updateUserValid = (req, res, next) => {
    const error = validateUserPartial(req.body, next);
    if (error) {
        throw { message: error, status: 400 };
    }
   
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;