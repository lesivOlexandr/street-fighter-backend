const { fight } = require('../models/fight');
const { validateFight } = require('../validators/fight.validator');

const createFightValid = (req, res, next) => {
    const error = validateFight(req.body);
    if(error) {
        throw { message: error, status: 400 };
    }
    next();
}

exports.createFightValid = createFightValid;