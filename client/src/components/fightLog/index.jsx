import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

import { getFights } from './../../services/domainRequest/fightRequest';
import './logsStyles.css';

export default class FightLog extends React.Component {
    state = {
        logs: [],
        isExpandent: false
    }

    onExpand = async () => {
        if(!this.state.isExpandent){
            try {
                const fights = await getFights();
                if(!fights.error) {
                    this.setState({ logs: fights || [], isExpandent: true });
                }
            } catch (err) {}
        } else {
            this.setState({ isExpandent: false })
        }
       
    }

    render() {
        const { logs } = this.state;
        return (
            <ExpansionPanel>
                <div onClick={this.onExpand}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                    >
                        <Typography>See combat log</Typography>
                    </ExpansionPanelSummary>
                </div>
                <ExpansionPanelDetails>
                {logs.map(log => (
                    <ExpansionPanel key={log.id}>                
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                        >
                            <Typography>Fight Between { log.fighter1.name } and { log.fighter2.name }</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            {this.buildFightxLogs(log)}
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                ))}
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }

    buildFightxLogs( fightInfo ) {
        return fightInfo.log.map(fightLog => (
            <>
                <Typography>
                    <span className="log-bold">
                        { ' ' + fightInfo.fighter2.name }    
                    </span> punched
                            
                    <span className="log-bold">
                        { ' ' + fightInfo.fighter1.name }   
                    </span>  for

                    <span className="log-hightlight">
                        { ' ' + fightLog.fighter2Shot.toFixed(1) } 
                    </span>
                </Typography>
                <Typography>
                    <span className="log-bold">
                        { ' ' + fightInfo.fighter1.name }    
                    </span> punched
                            
                    <span className="log-bold">
                        { ' ' + fightInfo.fighter2.name }   
                    </span>  for

                    <span className="log-hightlight">
                        { ' ' + fightLog.fighter1Shot.toFixed(1) } 
                    </span>
                </Typography>
                <Typography>
                    <span className="log-bold"> { fightInfo.fighter1.name } </span> 
                    health: <span className="log-hightlight">{ fightLog.fighter1Health.toFixed(1) }</span>
                </Typography>
                <Typography>
                    <span className="log-bold"> { fightInfo.fighter2.name } </span> 
                    health: <span className="log-hightlight">{ fightLog.fighter2Health.toFixed(1) }</span>
                </Typography>
            </>
        ));
    }
}