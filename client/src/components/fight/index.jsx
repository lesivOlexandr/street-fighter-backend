import React from 'react';
import { getFighters } from '../../services/domainRequest/fightersRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';
import { fight } from '../../helpers/fight-helpers';
import { createFight } from '../../services/domainRequest/fightRequest';

import './fight.css';
import FightLogs from './../fightLog/index';

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        isFightStarted: false
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    onFightStart = async () => {
        if(this.state.isFightStarted) {
            alert("You can't start new fight right now");
            return;
        }
        if(!this.state.fighter1 || !this.state.fighter2) {   
            alert('You should choose 2 fighters');
            return;
        }
        
        this.setState({ isFightStarted: true });
        const { winner, fightLogs } = await fight({ ...this.state.fighter1 }, { ...this.state.fighter2 }, this.setState.bind(this));

        createFight(fightLogs);

        setTimeout(()=> {
            alert(`Congratulations fighter ${ winner.name } win!`);
            this.setState({ fighter1: null, fighter2: null, isFightStarted: false });
        }, 200);
        
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({ fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        if(this.state.isFightStarted) {
            return [];
        }
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        if(this.state.isFightStarted) {
            return [];
        }
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    render() {
        const  { fighter1, fighter2 } = this.state;
        return (
            <>
                <div id="wrapper">
                    <NewFighter onCreated={this.onCreate} />
                    <div id="figh-wrapper">
                        <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                        <div className="btn-wrapper">
                            <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                        </div>
                        <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                    </div>
                </div>
                <FightLogs></FightLogs>
            </>
        );
    }
}

export default Fight;