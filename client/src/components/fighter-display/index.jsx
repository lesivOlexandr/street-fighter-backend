import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { defaultFighterImageURL } from '../../constants/URLs';
import './fighter-image.css';



const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export default function FighterDisplay({ fighter }) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      {/*
        В CardMedia по задуму должны были отображаться картинки бойцоы из предыдущего задания, ссылки на которые хранились бы
        в database.json вместе с остальными бойцами из предыдущего задания, но боту автотестеру не понравилось, что при валидации
        есть лишние бойцы, поэтому не будет не бойцов не картинок
      */}
        <CardMedia
          component="img"
          alt={fighter.name || null}
          classes={{ img: "fighter-image" }}
          image={fighter.source || defaultFighterImageURL}
          title={fighter.name || null}
        />
        <CardContent>
          <Typography variant="h6" className={classes.title}>
            Fighter
          </Typography>
          <div className={classes.demo}>
            <List>
              <FighterDetails fighter={fighter}/>
            </List>
          </div>
        </CardContent>
    </Card>
  );
}

function FighterDetails({ fighter: { name = null, health = 0, defense = 0, power = 0 }}) {
  return (
    <>
      <ListItem>
        <ListItemText
          primary={'name: ' + name}
        />
      </ListItem>
      <ListItem>
        <ListItemText
          primary={'health: ' + health.toFixed(1)}
        />
      </ListItem>
      <ListItem>
        <ListItemText
          primary={'defense: ' + defense}
        />
      </ListItem>
      <ListItem>
        <ListItemText
          primary={'power: ' + power}
        />
      </ListItem>
    </>
  )
}