export async function fight(fighter1, fighter2, stateFunction) {
    const fightLogs = {
        fighter1: fighter1.id,
        fighter2: fighter2.id,
        log: [

        ]
    }
    const promise = new Promise(resolve => {

        const IntervalId = setInterval(() => {
            const damage1 = getDamage(fighter1, fighter2);
            const damage2 = getDamage(fighter2, fighter1);
    
            fighter2.health = fighter2.health - damage1 < 0 ? 0: fighter2.health - damage1;
            fighter1.health = fighter1.health - damage2 < 0 ? 0: fighter1.health - damage2;

            fightLogs.log.push({ 
                fighter1Health: fighter1.health,
                fighter2Health: fighter2.health,
                fighter1Shot: damage1,
                fighter2Shot: damage2
            })

    
            stateFunction({ fighter1: { ...fighter1 }, fighter2: { ...fighter2 } });

            if (fighter1.health <= 0) {
                clearInterval(IntervalId);
                resolve({ winner: fighter2, fightLogs });
            }
    
            if (fighter2.health <= 0) {
                clearInterval(IntervalId);
                resolve({ winner: fighter1, fightLogs });
            };
            
        }, 1000)
    });
    return promise;
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;
  
  return damage > 0 ? damage: 0;
}

export function getHitPower(fighter) {
  const { power } = fighter;
  const criticalHitChance = 1 + Math.random();

  const attackPower = power * criticalHitChance;

  return attackPower;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = 1 + Math.random();

  const blockPower = defense * dodgeChance;

  return blockPower; 
}

