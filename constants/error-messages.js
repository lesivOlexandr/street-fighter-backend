const messages = {
  NoUsersFound: "No users found",
  NoFightersFound: "No fighters found",
  UserNotFound: "Cannot find user with the given id",
  FighterNotFound: "Cannot find fighter with the given id",
  FightsNotFound: "Cannot find any fights",

  CriticalError: "An error occurred on the server",
  HasExtraField: "Entity has extra fields",

  InvalidEmail: "Given email is not valid",
  UserEmailNotUnique: "User with given email already exists",

  InvalidPhoneNumber: "Given phone number is not valid",
  UserPhoneNumberNotUnique: "User with given phone already exists",
  
  InvalidPassword: "Given password is not valid",

  InvalidLastName: "Given last name is not valid",
  InvalidFirstName: "Given first name is not valid",
  ShouldNotContaineId: "Given entity should not contain id property",


  InvalidFighterName: "Fighter name should not be empty",
  InvalidHealth: "Given health property is invalid",
  InvalidPower: "Given power property is invalid",
  InvalidDefense: "Given defense property is invalid",

  FightlogEmpty: "Provided fight log if empty",
  InvalidFightLog: "Provided fight log is invalid"
};

module.exports.messages = Object.freeze(messages);