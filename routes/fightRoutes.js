const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid } = require('../middlewares/fights.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.get('/', (req, res, next) => {
  res.data = FightService.getAll();
  next();
})

router.post('/', createFightValid, (req, res, next) => {
  res.data = FightService.createOne(req.body);
  next();
});

router.use(responseMiddleware);

module.exports = router;