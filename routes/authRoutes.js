const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const data = AuthService.login(req.query);
        res.data = data;
        next();
    } catch(error) {
        throw { message: error.message, status: 404 }
    }
}, responseMiddleware);

module.exports = router;