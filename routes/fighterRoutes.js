const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { errorHandlerMiddleware } = require('../middlewares/error-handler.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    res.data = FighterService.getAll();
    next();
})

router.get('/:id', (req, res, next) => {
    res.data = FighterService.getOne(req.params.id);
    next();
})

router.post('/', createFighterValid, (req, res, next) => {
    res.data = FighterService.createOne(req.body);
    next();
})

router.put('/:id', updateFighterValid, (req, res, next) => {
    res.data = FighterService.updateOne(req.params.id, req.body);
    next();
})

router.delete('/:id', (req, res, next) => {
    res.data = FighterService.deleteOne(req.params.id);
    next();
})

router.use(responseMiddleware);

module.exports = router;