const { UserRepository } = require('../repositories/userRepository');
const { messages } = require('../constants/error-messages');

class UserService {
    getAll() {
        const users = UserRepository.getAll();
        return users;
    }

    createOne(user) {
        return UserRepository.create(user);
    }

    updateOne(id, datatoUpdate) {
        if(!this.isExist(id)){
            throw { message: messages.UserNotFound, status: 404 };
        }
        return UserRepository.update(id, datatoUpdate);
    }

    deleteOne(id) {
        if(!this.isExist(id)){
            throw { message: messages.UserNotFound, status: 404 };
        }
        
        return UserRepository.delete(id);
    }

    getOne(id) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw { message: messages.UserNotFound, status: 404 };
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    isExist(id) {
        return !!this.getOne(id);
    }
}

module.exports = new UserService();