const { FighterRepository } = require('../repositories/fighterRepository');
const { messages } = require('../constants/error-messages')

class FighterService {
    getAll() {
        const fighters = FighterRepository.getAll();
        return fighters;
    }

    createOne(fighter) {
        return FighterRepository.create(fighter);
    }

    updateOne(id, datatoUpdate) {
        if(!this.isExist(id)){
            throw { message: messages.FighterNotFound, status: 404 };
        }
        return FighterRepository.update(id, datatoUpdate);
    }

    deleteOne(id) {
        if(!this.isExist(id)){
            throw { message: messages.FighterNotFound, status: 404 };
        }
        return FighterRepository.delete(id);
    }

    getOne(id) {
        const fighter = FighterRepository.getOne({ id });
        if(!fighter){
            throw { message: messages.FighterNotFound, status: 404 };
        }
        return fighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    isExist(id) {
        return !!this.getOne(id);
    }
}

module.exports = new FighterService();