const { FightRepository } = require('../repositories/fightRepository');
const FighterService = require('../services/fighterService');
const { messages } = require('../constants/error-messages');

class FightsService {
    getAll() {
        let fights = FightRepository.getAll();

        fights = fights.map(this.replaceFightersIdWithInstanses);
        return fights;
    }

    replaceFightersIdWithInstanses({ ...fight }) {
        // Свойства из fight сначала копируем, ведь данные хранятся памяти и если мы здесь измменим изначальные свойства из fight,
        // то при всех последующих запросах, в fight вместо id бойца будет объект бойца

        // Заменяем id бойцов на их сущности
        // В настоящей базе данных нужно было б использовать JOIN(или что-то подобное специфичное для базы),
        // и желательно с триггерами на удаление бойцов
        fight.fighter1 = FighterService.getOne(fight.fighter1);
        fight.fighter2 = FighterService.getOne(fight.fighter2);
        return fight
    }

    createOne(fight) {
        return FightRepository.create(fight);
    }
}

module.exports = new FightsService();