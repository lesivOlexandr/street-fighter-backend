const { fight } = require('../models/fight');
const { messages } = require('../constants/error-messages');
const FighterService = require('../services/fighterService');

module.exports.validateFight = function(fight){
    if('id' in fight) {
        return messages.ShouldNotContaineId;
    }
    if(!fight.fighter1) {
        return messages.InvalidFighterName;
    }
    if(!fight.fighter2) {
        return messages.InvalidFighterName;
    }

    if(
        !FighterService.isExist(fight.fighter1) || 
        !FighterService.isExist(fight.fighter2)
    ) {
       return messages.FighterNotFound;
    }


    if(!(fight.log instanceof Array) || fight.log.length == 0) {
        return messages.FightlogEmpty;
    }

    fight.log.forEach(log => {
        // Скорее всего в логах будут приходить числа с плавающей точкой, их дополнительные преобразования в число не нужны,
        // но существует вероятность, что число целое, 
        // тогда express будет рассматривать его как строку и нужно преобразовать в число 
        log.fighter1Shot = +log.fighter1Shot;
        log.fighter2Shot = +log.fighter2Shot;
        log.fighter1Health = +log.fighter1Health;
        log.fighter2Health = +log.fighter2Health;
        if(
            isNaN(log.fighter1Shot) || isNaN(log.fighter2Shot) ||
            isNaN(log.fighter1Health) || isNaN(log.fighter2Health)
        ) {
            return messages.InvalidFightLog;
        }
    })
    return null;
}