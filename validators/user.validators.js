const { messages } = require('../constants/error-messages');
const { user: userModel } = require('../models/user');

module.exports.validateUser = validateUser;
module.exports.validateUserPartial = validateUserPartial;

module.exports.isEmailValid = isEmailValid;
module.exports.isPhoneNumberValid = isPhoneNumberValid;
module.exports.isPasswordValid = isPasswordValid;
module.exports.isNameValid = isNameValid;

function validateUser(user) {
    if('id' in user) {
        return messages.ShouldNotContaineId;
    }

    if(!isEmailValid(user.email)) {
        return messages.InvalidEmail;
    }

    if(!isPhoneNumberValid(user.phoneNumber)) {
        return messages.InvalidPhoneNumber;
    }

    if(!isPasswordValid(user.password)) {
        return messages.InvalidPassword;
    }

    if(!isNameValid(user.firstName)) {
        return messages.InvalidFirstName;
    }

    if(!isNameValid(user.lastName)) {
        return messages.InvalidLastName;
    }

    for (let key in user) {
        if(!(key in userModel)){
            return messages.HasExtraField;
        }
    } 
    
    return null;
}

function validateUserPartial(userPart) {
    if('id' in userPart) {
        return messages.ShouldNotContaineId;
    }

    // Если в userPart есть свойство name, но оно null/undefined/пустая строка
    if('email' in userPart && !isEmailValid(userPart.email)) {
        return messages.InvalidEmail;
    }

    if('phoneNumber' in userPart && !isPhoneNumberValid(userPart.phoneNumber)) {
        return messages.InvalidPhoneNumber;
    }

    if('password' in userPart && !isPasswordValid(userPart.password)) {
        return messages.InvalidPassword;
    }

    if('firstName' in userPart && !isNameValid(userPart.firstName)) {
        return messages.InvalidFirstName;
    }

    if('lastName' in userPart && !isNameValid(userPart.lastName)) {
        return messages.InvalidLastName;
    }

    for (let key in userPart) {
        if(!(key in userModel)){
            return messages.HasExtraField;
        }
    }

    return null;
}

function isEmailValid(email) {
    return (
        email && 
        typeof email === 'string' &&
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/.test(email)
    );
}

function isPhoneNumberValid(phoneNumber) {
    return (
        phoneNumber &&
        typeof phoneNumber === 'string' &&
        /^\+380[0-9]{9}$/.test(phoneNumber)   
    );
}

function isPasswordValid(password) {
    return (
        password &&
        typeof password === 'string' &&
        password.length >= 3 && 
        password.length < 256
   )
}

function isNameValid(name){
    return name && name.length < 256;
}