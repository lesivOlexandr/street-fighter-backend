const { fighter: fighterModel } = require('../models/fighter');
const { messages } = require('../constants/error-messages');

module.exports.validateFighter = validateFighter;
module.exports.validateFighterPartial = validateFighterPartial;

module.exports.isNameValid = isNameValid;
module.exports.isHealthValid = isHealthValid;
module.exports.isPowerValid = isPowerValid;
module.exports.isDefenseValid = isDefenseValid;

function validateFighter(fighter) {
    if('id' in fighter) {
        return messages.ShouldNotContaineId;
    }

    if(!isNameValid(fighter.name)){
        return messages.InvalidFighterName;
    }
    // Преобразовываем из строкового типа в числовой, так как из запроса приходят строки
    // Если значение было undefined, не числовой строкой и т.д., то после преобразования будет NaN
    fighter.health = +fighter.health || 100;
    fighter.defense = +fighter.defense;
    fighter.power = +fighter.power;

    if(!isDefenseValid(fighter.defense)) {
        return messages.InvalidDefense;
    }
    if(!isPowerValid(fighter.power)) {
        return messages.InvalidPower;
    }
    
    for (let key in fighter) {
        if(!(key in fighterModel)){
            return messages.HasExtraField;
        }
    }

    return null;
}

function validateFighterPartial(fighterPart) {
    if('id' in fighterPart) {
        return messages.ShouldNotContaineId;
    }
    
    // Если в figterPart есть свойство name, но оно null/undefined/пустая строка
    if('name' in fighterPart && !isNameValid(fighterPart.name)) {
        return messages.InvalidFighterName;
    }

    if('defense' in fighterPart) {
        fighterPart.defense = +fighterPart.defense;

        if(!isDefenseValid(fighterPart.defense)){
            return messages.InvalidDefense;
        }
    }

    if('power' in fighterPart) {
        fighterPart.power = +fighterPart.power;

        if(!isPowerValid(fighterPart.power)) {
            return messages.InvalidPower; 
        }
    }

    if('health' in fighterPart) {
        fighterPart.health = +fighter.health;

        if(!isHealthValid(fighterPart.health)) {
            return messages.InvalidHealth;
        }
    }

    for (let key in fighterPart) {
        if(!(key in fighterModel)){
            return messages.HasExtraField;
        }
    }

    return null;
}

function isNameValid(name) {
    return name && name.length < 256;
}

function isDefenseValid(defense) {
    return (
        !isNaN(defense) &&
        defense >= 1 &&
        defense <= 10
    );
}

function isPowerValid(power) {
    return (
        !isNaN(power) &&
        power > 0 &&
        power < 100
    );
}

function isHealthValid(health) {
    return (
        !isNaN(health) &&
        health > 0 &&
        health < 100
    );
}